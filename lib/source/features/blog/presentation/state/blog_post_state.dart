part of '../view_model/blog_post_view_model.dart';

class BlogPostState extends Equatable {
  const BlogPostState({
    this.titleInput = '',
    this.subtitleInput = '',
    this.bodyInput = '',
    this.date = '',
    this.status = BlogPostStatus.initial,
    this.deleteStatus = BlogPostStatus.initial,
    this.blogPosts = const [],
  });

  final String titleInput;
  final String subtitleInput;
  final String bodyInput;
  final String date;
  final BlogPostStatus status;
  final BlogPostStatus deleteStatus;
  final List<BlogPost> blogPosts;

  @override
  List<Object?> get props => [titleInput, subtitleInput, bodyInput, date, status, deleteStatus, blogPosts];

  BlogPostState copyWith({
    String? titleInput,
    String? subtitleInput,
    String? bodyInput,
    String? date,
    BlogPostStatus? status,
    BlogPostStatus? deleteStatus,
    List<BlogPost>? blogPosts
  }) {
    return BlogPostState(
      titleInput: titleInput ?? this.titleInput,
      subtitleInput: subtitleInput ?? this.subtitleInput,
      bodyInput: bodyInput ?? this.bodyInput,
      date: date ?? this.date,
      status: status ?? this.status,
      deleteStatus: deleteStatus ?? this.deleteStatus,
      blogPosts: blogPosts ?? this.blogPosts
    );
  }
}

enum BlogPostStatus { initial, loading, success, error }