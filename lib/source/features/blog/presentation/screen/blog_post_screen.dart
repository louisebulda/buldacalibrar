import 'package:first_app/source/features/blog/data/di/blog_post_service_locator.dart';
import 'package:first_app/source/features/blog/presentation/components/add_button.dart';
import 'package:first_app/source/features/blog/presentation/components/blog_post_item.dart';
import 'package:first_app/source/features/blog/presentation/components/body_textfield.dart';
import 'package:first_app/source/features/blog/presentation/components/subtitle_textfield.dart';
import 'package:first_app/source/features/blog/presentation/components/title_textfield.dart';
import 'package:first_app/source/features/blog/presentation/view_model/blog_post_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BlogPostScreen extends StatelessWidget {
  const BlogPostScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return BlocProvider<BlogPostViewModel>(
      create: (context) => BlogPostViewModel(repository: blogPostRepository)
      // For initialization
        ..add(BlogPostsSubscriptionRequested())
        ..add(BlogPostsRequested()),
      child: Padding(
        padding: EdgeInsets.zero,
        child: SingleChildScrollView(
          child: Column(
            children: [ 
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(width: width/2 - 32, child: const BlogTitleTextField()),
                      SizedBox(width: width/2 - 32,child: const BlogSubtitleTextField())
                    ],
                  ),
                  SizedBox(width: width/2 - 32, child: const BlogBodyTextField())
                ],
              ),
                  
              // Add Post Button
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: Center(
                  child: AddPostButton()
                ),
              ),
            
              // Add List Items
              BlocBuilder<BlogPostViewModel, BlogPostState>(
                buildWhen: (previous, current) => current.status != previous.status,
                builder: (context, state) {
                  if (state.status == BlogPostStatus.loading) {
                    return const Padding(
                      padding: EdgeInsets.all(16.0),
                      child: CircularProgressIndicator(),
                    );
                  }
            
                  return ListView.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    padding: const EdgeInsets.all(16.0),
                    itemBuilder: (context, index) {
                      final blogPost = state.blogPosts[index];
                      
                      return BlogPostItem(blogPostItem: blogPost);
                    },
                    itemCount: state.blogPosts.length,
                  );
                }),
              ],
            ),
          ),
        ),
    );
  }
}