import 'package:equatable/equatable.dart';
import 'package:first_app/source/features/blog/data/models/blog_post.dart';
import 'package:first_app/source/features/blog/data/models/blog_post_input.dart';
import 'package:first_app/source/features/blog/data/repository/blog_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

part '../event/blog_post_event.dart';
part '../state/blog_post_state.dart';

class BlogPostViewModel extends Bloc<BlogPostEvent, BlogPostState> {
  BlogPostViewModel({
    required BlogPostRepository repository,
  }) : _repository = repository,
      super(const BlogPostState()) {
        on<BlogPostsSubscriptionRequested>(_onSubscriptionRequested);
        on<BlogPostsRequested>(_onBlogPostsRequested);
        on<BlogPostsSubmitted>(_onBlogPostsSubmitted);
        on<BlogPostTitleChanged>(_onTitleChanged);
        on<BlogPostSubtitleChanged>(_onSubtitleChanged);
        on<BlogPostBodyChanged>(_onBodyChanged);
        on<BlogPostsDeleted>(_onBlogPostDeleted);
      }

  final BlogPostRepository _repository;
  
  Future<void> _onSubscriptionRequested(
    BlogPostsSubscriptionRequested event,
    Emitter<BlogPostState> emit,
  ) async {
    emit(state.copyWith(status: BlogPostStatus.loading));

    await emit.forEach<List<BlogPost>> (
      _repository.getBlogPosts(),
      onData: (blogPosts) {
        return state.copyWith(
          status: BlogPostStatus.success,
          blogPosts: blogPosts.toList()
        );
      }, 
      onError: (_, __) => state.copyWith(
        status: BlogPostStatus.error
      )
    );
  }

  Future<void> _onBlogPostsRequested(
    BlogPostsRequested event,
    Emitter<BlogPostState> emit,
  ) async {
    try {
      final blogPosts = await _repository.load();

      emit(state.copyWith(
        status: BlogPostStatus.success,
        blogPosts: blogPosts.toList()
      ));
    } on Exception catch (_) {
      state.copyWith(status: BlogPostStatus.error);
    }
  }

  Future<void> _onBlogPostsSubmitted(
    BlogPostsSubmitted event,
    Emitter<BlogPostState> emit,
  ) async {
    if (state.titleInput.isEmpty || state.subtitleInput.isEmpty || state.bodyInput.isEmpty) {
      emit(state.copyWith(status: BlogPostStatus.error));
      return;
    }

    emit(state.copyWith(status: BlogPostStatus.loading));

    String currentDate = DateFormat('MMM dd, yyyy').format(DateTime.now());


    final blogPost = await _repository.addBlogPost(
      BlogPostInput(
        title: state.titleInput, 
        subtitle: state.subtitleInput, 
        body: state.bodyInput, 
        id: '${state.blogPosts.length + 1}',
        date: currentDate
        )
    );

    emit(state.copyWith(
      status: BlogPostStatus.success,
      blogPosts: [
        ...state.blogPosts,
        blogPost
      ]
    ));
  }

  Future<void> _onBlogPostDeleted(
    BlogPostsDeleted event,
    Emitter<BlogPostState> emit
  ) async {
    emit(state.copyWith(status: BlogPostStatus.loading));

    await _repository.deleteBlogPost(event.id);
    emit(state.copyWith(deleteStatus: BlogPostStatus.success));
  }

  void _onTitleChanged(
    BlogPostTitleChanged event,
    Emitter<BlogPostState> emit,
  ) {
    emit(state.copyWith(titleInput: event.title));
  }

  void _onSubtitleChanged(
    BlogPostSubtitleChanged event,
    Emitter<BlogPostState> emit,
  ) {
    emit(state.copyWith(subtitleInput: event.subtitle));
  }

  void _onBodyChanged(
    BlogPostBodyChanged event,
    Emitter<BlogPostState> emit,
  ) {
    emit(state.copyWith(bodyInput: event.body));
  }
}