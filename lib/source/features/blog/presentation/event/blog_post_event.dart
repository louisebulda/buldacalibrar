part of '../view_model/blog_post_view_model.dart';

abstract class BlogPostEvent {}

class BlogPostsSubscriptionRequested extends BlogPostEvent {}

class BlogPostsRequested extends BlogPostEvent {}

class BlogPostsSubmitted extends BlogPostEvent {}

class BlogPostsDeleted extends BlogPostEvent {
  final String id;

  BlogPostsDeleted(this.id);
}

class BlogPostTitleChanged extends BlogPostEvent {
  final String title;

  BlogPostTitleChanged(this.title);
}

class BlogPostSubtitleChanged extends BlogPostEvent {
  final String subtitle;

  BlogPostSubtitleChanged(this.subtitle);
}

class BlogPostBodyChanged extends BlogPostEvent {
  final String body;

  BlogPostBodyChanged(this.body);
}

