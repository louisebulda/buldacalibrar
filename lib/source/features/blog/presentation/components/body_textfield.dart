// ignore_for_file: prefer_const_constructors

import 'package:first_app/source/features/blog/presentation/view_model/blog_post_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BlogBodyTextField extends StatelessWidget {
  const BlogBodyTextField({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: SizedBox(
            child: TextFormField(
              decoration: const InputDecoration(
                border: UnderlineInputBorder(),
                contentPadding: EdgeInsets.zero,
                labelText: 'Body',
                labelStyle: TextStyle(
                  color: Colors.black,
                  fontSize: 16
                )
              ),
              style: TextStyle(
                color: Colors.black,
                fontSize: 16),
              onChanged: (value) {
                context.read<BlogPostViewModel>().add(
                  BlogPostBodyChanged(value)
                );
              },
            ),
                ),
        )
      ]
    );
  }
}