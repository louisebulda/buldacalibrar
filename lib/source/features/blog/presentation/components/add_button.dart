// ignore_for_file: prefer_const_constructors

import 'package:first_app/source/features/blog/presentation/view_model/blog_post_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

class AddPostButton extends StatelessWidget {
  const AddPostButton({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlogPostViewModel, BlogPostState>(
      buildWhen: (previous, current) => previous.status != current.status,
      builder: (context, state) {
        return ElevatedButton(
          onPressed: () {
            if (state.status == BlogPostStatus.loading) {
              return;
            }

            context.read<BlogPostViewModel>().add(BlogPostsSubmitted());           
          },        
          style: ElevatedButton.styleFrom(          
            backgroundColor: Colors.deepPurple,
            foregroundColor: Colors.white,
            textStyle: TextStyle(
              fontFamily: GoogleFonts.josefinSans().fontFamily
            ),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
            elevation: 0.0,
            shadowColor: Colors.transparent
          ),
          child: Text('Add Post')
      );
      },
    );
  }
}