import 'package:first_app/source/features/blog/data/models/blog_post.dart';
import 'package:first_app/source/features/blog/presentation/view_model/blog_post_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

class BlogPostItem extends StatefulWidget {
BlogPostItem({
    Key? key,
    required this.blogPostItem,
  }) : super(key: key);

  final BlogPost? blogPostItem;

  @override
  _BlogPostItemState createState() => _BlogPostItemState(blogPostItem: blogPostItem);
}

class _BlogPostItemState extends State<BlogPostItem> {
  _BlogPostItemState({
    required this.blogPostItem,
  });

  final BlogPost? blogPostItem;
  bool isExpanded = false;

  @override
  Widget build(BuildContext context) {
    return Card(
      color: const Color(0xFFF8F8F8),
      elevation: 0,
      shadowColor: Colors.transparent,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListTile(
          leading: CircleAvatar(
            backgroundColor: Colors.deepPurple,
            child: Text(blogPostItem?.id ?? '', style: TextStyle(
              fontFamily: GoogleFonts.josefinSans().fontFamily,
              color: Colors.white
            ))
          ),
          
          title: Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Title
                Text(blogPostItem?.title ?? '', style: TextStyle(
                  fontFamily: GoogleFonts.josefinSans().fontFamily,
                  fontWeight: FontWeight.bold,
                  fontSize: 24,
                  color: Colors.deepPurple
                )),
            
                // Subtitle and Date
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(blogPostItem?.subtitle ?? '', style: TextStyle(
                      fontFamily: GoogleFonts.josefinSans().fontFamily,
                      fontWeight: FontWeight.normal,
                      fontSize: 16,
                      color: Colors.deepPurple
                    )),

                    Text(blogPostItem?.date ?? '', style: TextStyle(
                      fontFamily: GoogleFonts.josefinSans().fontFamily,
                      fontWeight: FontWeight.normal,
                      fontSize: 14,
                      color: Colors.deepPurple
                    ))
                  ],
                )
              ],
            ),
          ),

          // Body
          subtitle: Container(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Text(blogPostItem?.body ?? '', style: TextStyle(
                fontFamily: GoogleFonts.josefinSans().fontFamily,
                fontWeight: FontWeight.normal,
                fontSize: 14,
                color: Colors.black,
                wordSpacing: 1.3), 
                textAlign: TextAlign.justify,
                maxLines:  isExpanded ? null : 4,
                overflow: isExpanded ? TextOverflow.visible : TextOverflow.ellipsis,
            )
          ),

          // Delete Button
          trailing: ElevatedButton(
            style: ElevatedButton.styleFrom(          
              backgroundColor: Colors.transparent,
              foregroundColor: Colors.white,
              elevation: 0.0,
              shadowColor: Colors.transparent
            ),
            onPressed: () {
              context.read<BlogPostViewModel>().add(
                  BlogPostsDeleted(blogPostItem?.id ?? '')
                );
            },
            child: const Icon(Icons.delete, color: Colors.red)
          ),

          hoverColor: Colors.transparent,
          onTap: () {
            setState(() {
              isExpanded = !isExpanded;
            });
          },
        ),
      ),
    );
  }
}