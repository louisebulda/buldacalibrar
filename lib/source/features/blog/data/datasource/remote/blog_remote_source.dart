import 'package:first_app/source/features/blog/data/models/blog_post.dart';
import 'package:first_app/source/features/blog/data/models/blog_post_input.dart';

class BlogRemoteSource {
  
  final List<BlogPost> _blogPosts = [
    const BlogPost(
      title: '1989', 
      subtitle: 'Taylor\'s Version',         
      body: 'It is the fourth re-recorded album by the American singer-songwriter Taylor Swift. It is a re-recording of Swift\'s fifth studio album, 1989, and was released on October 27, 2023, by Republic Records. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lacinia felis nec aliquam vulputate. Etiam varius lectus in orci ornare, sed efficitur nibh luctus. Pellentesque eu ullamcorper diam. Suspendisse et nulla tristique, blandit mi sed, tincidunt dolor. Cras in facilisis nisl. In finibus finibus purus, a elementum ligula sagittis eget. Donec a tellus quis dui facilisis dapibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque pretium at libero et fermentum. Mauris facilisis, sem et gravida commodo, sapien lorem rhoncus dolor, ac vehicula arcu justo ut velit. Proin rhoncus sed justo sed aliquet.', 
      id: '1',
      date: 'Jan 15, 2024'
    ),

    const BlogPost(
      title: 'Lover', 
      subtitle: 'Originally Owned',         
      body: 'It is the seventh studio album by the American singer-songwriter Taylor Swift. It was released on August 23, 2019, through Republic Records, and is her first album after her departure from Big Machine Records, which resulted in a highly publicized dispute. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lacinia felis nec aliquam vulputate. Etiam varius lectus in orci ornare, sed efficitur nibh luctus. Pellentesque eu ullamcorper diam. Suspendisse et nulla tristique, blandit mi sed, tincidunt dolor. Cras in facilisis nisl. In finibus finibus purus, a elementum ligula sagittis eget. Donec a tellus quis dui facilisis dapibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque pretium at libero et fermentum. Mauris facilisis, sem et gravida commodo, sapien lorem rhoncus dolor, ac vehicula arcu justo ut velit. Proin rhoncus sed justo sed aliquet.', 
      id: '2',
      date: 'Jan 15, 2024'
    ),

    const BlogPost(
      title: 'Reputation', 
      subtitle: 'Stolen Version',         
      body: 'It is the sixth studio album by the American singer-songwriter Taylor Swift, released on November 10, 2017, and it was her last album under Big Machine Records. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lacinia felis nec aliquam vulputate. Etiam varius lectus in orci ornare, sed efficitur nibh luctus. Pellentesque eu ullamcorper diam. Suspendisse et nulla tristique, blandit mi sed, tincidunt dolor. Cras in facilisis nisl. In finibus finibus purus, a elementum ligula sagittis eget. Donec a tellus quis dui facilisis dapibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque pretium at libero et fermentum. Mauris facilisis, sem et gravida commodo, sapien lorem rhoncus dolor, ac vehicula arcu justo ut velit. Proin rhoncus sed justo sed aliquet.', 
      id: '3',
      date: 'Jan 15, 2024'
    ),

    const BlogPost(
      title: 'Speak Now', 
      subtitle: 'Taylor\'s Version',         
      body: 'It is the third re-recorded album by the American singer-songwriter Taylor Swift. It is a re-recording of Swift\'s third studio album, Speak Now, part of Swift\'s counteraction to a 2019 masters dispute regarding the ownership of her back catalog. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lacinia felis nec aliquam vulputate. Etiam varius lectus in orci ornare, sed efficitur nibh luctus. Pellentesque eu ullamcorper diam. Suspendisse et nulla tristique, blandit mi sed, tincidunt dolor. Cras in facilisis nisl. In finibus finibus purus, a elementum ligula sagittis eget. Donec a tellus quis dui facilisis dapibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque pretium at libero et fermentum. Mauris facilisis, sem et gravida commodo, sapien lorem rhoncus dolor, ac vehicula arcu justo ut velit. Proin rhoncus sed justo sed aliquet.', 
      id: '4',
      date: 'Jan 15, 2024'
    ),
  ];

  Future<List<BlogPost>> getBlogPosts() async {
    // Create Mock API Call
    return await Future.delayed(
      const Duration(seconds: 2), () {
        return _blogPosts;
      } 
    );
  }

  Future<BlogPost> addBlogPost(BlogPostInput input) async {
    return await Future.delayed(
      const Duration(seconds: 2), 
      () => BlogPost(
        title: input.title, 
        subtitle: input.subtitle, 
        body: input.body, 
        id: input.id,
        date: input.date
        )
    );
  }
  
  Future<void> deleteBlogPost(String id) async {
    return await Future.delayed(
      const Duration(seconds: 2), () {
        return;
      } 
    );
  }
}