import 'package:equatable/equatable.dart';

class BlogPostInput extends Equatable {
  final String title;
  final String subtitle;
  final String body;
  final String id;
  final String date;

  const BlogPostInput({
    required this.title,
    required this.subtitle,
    required this.body,
    required this.id,
    required this.date
  });

  Map<String, dynamic> toJson() {
    return {
      'title': title,
      'subtitle': subtitle,
      'body': body,
      'id': id,
      'date': date
    };
  }

  @override
  List<Object?> get props => [title, subtitle, body, id, date];
}