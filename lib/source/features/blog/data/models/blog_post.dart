import 'package:equatable/equatable.dart';

class BlogPost extends Equatable {
  final String title;
  final String subtitle;
  final String body;
  final String id;
  final String date;

  const BlogPost({
    required this.title,
    required this.subtitle,
    required this.body,
    required this.id,
    required this.date
  });

  factory BlogPost.fromJson(Map<String, dynamic> json) {
    return BlogPost(
      title: json['title'] as String? ?? '', 
      subtitle: json['subtitle'] as String? ?? '', 
      body: json['body'] as String? ?? '', 
      id: json['id'] as String? ?? '',
      date: json['date'] as String? ?? ''
    );
  }

  @override
  List<Object?> get props => [title, subtitle, body, id, date];
}