import 'package:first_app/source/features/blog/data/datasource/remote/blog_remote_source.dart';
import 'package:first_app/source/features/blog/data/repository/blog_repository_implem.dart';

final blogPostRemoteSource = BlogRemoteSource();
final blogPostRepository = BlogPostRepositoryImplem(blogPostRemoteSource);
