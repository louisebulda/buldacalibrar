import 'dart:async';

import 'package:first_app/source/features/blog/data/datasource/remote/blog_remote_source.dart';
import 'package:first_app/source/features/blog/data/models/blog_post.dart';
import 'package:first_app/source/features/blog/data/models/blog_post_input.dart';
import 'package:first_app/source/features/blog/data/repository/blog_repository.dart';

class BlogPostRepositoryImplem extends BlogPostRepository {
  BlogPostRepositoryImplem(this._remoteSource);
  final BlogRemoteSource _remoteSource;

  final StreamController<List<BlogPost>> _streamController = 
  StreamController.broadcast();

  List<BlogPost> _blogPosts = [];

  @override
  Stream<List<BlogPost>> getBlogPosts() => _streamController.stream;

  @override
  Future<List<BlogPost>> load() async {
    final blogPosts = await _remoteSource.getBlogPosts();
    _blogPosts = blogPosts;
    _streamController.add(_blogPosts);

    return blogPosts;
  }
  
  @override
  Future<BlogPost> addBlogPost(BlogPostInput input) async {
    final blogPostInput = await _remoteSource.addBlogPost(input);

    _blogPosts.add(blogPostInput);
    _streamController.add(_blogPosts);

    return blogPostInput;
  }

  @override
  Future<void> deleteBlogPost(String id) async {
    await _remoteSource.deleteBlogPost(id);

    _blogPosts.removeWhere((element) => element.id == id);
    _streamController.add(_blogPosts);
    
    return Future.value();
  }
}