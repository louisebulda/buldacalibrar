import 'package:first_app/source/features/blog/data/models/blog_post.dart';
import 'package:first_app/source/features/blog/data/models/blog_post_input.dart';

abstract class BlogPostRepository {
  Stream<List<BlogPost>> getBlogPosts();

  Future<List<BlogPost>> load();

  Future<BlogPost> addBlogPost(BlogPostInput input);

  Future<void> deleteBlogPost(String id);
}