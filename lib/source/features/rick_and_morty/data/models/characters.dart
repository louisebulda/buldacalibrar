import 'package:equatable/equatable.dart';
import 'package:first_app/source/features/rick_and_morty/data/models/character_info.dart';
import 'package:first_app/source/features/rick_and_morty/data/models/page_info.dart';

class CharactersList extends Equatable {
  PageInfo? pageInfo;
  List<CharacterInfo>? results;

  CharactersList({
    required this.pageInfo,
    required this.results
  });

  CharactersList.fromJson(Map<String, dynamic> json) {
    pageInfo = json['info'] != null ? PageInfo.fromJson(json['info']) : null;

    if (json['results'] != null) {
      results = [];
      json['results'].forEach((value) {
        results!.add(CharacterInfo.fromJson(value));
      });
    }

    // return CharactersList(
    //   pageInfo: json['info'] as PageInfo, 
    //   results: json['results'] as List<CharacterInfo>
    // );
  }

  @override
  List<Object?> get props => [pageInfo, results];
}