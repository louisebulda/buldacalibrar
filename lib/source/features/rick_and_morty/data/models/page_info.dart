import 'package:equatable/equatable.dart';

class PageInfo extends Equatable {
  final int count;
  final int pages;
  final String next;
  final String prev;

  const PageInfo({
    required this.count,
    required this.pages,
    required this.next,
    required this.prev
  });

  factory PageInfo.fromJson(Map<String, dynamic> json) {
    return PageInfo(
      count: json['count'] as int? ?? 0, 
      pages: json['pages'] as int? ?? 0, 
      next: json['next'] as String? ?? '', 
      prev: json['prev'] as String? ?? '',
    );
  }

  @override
  List<Object?> get props => [count, pages, next, prev];
}