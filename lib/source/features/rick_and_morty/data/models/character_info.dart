import 'package:equatable/equatable.dart';

class CharacterInfo extends Equatable {
  int? id;
  String? name;
  String? status;
  String? species;
  String? type;
  String? gender;
  Location? origin;
  Location? location;
  String? image;
  List<String>? episodes;
  String? url;
  DateTime? created;

  CharacterInfo({
    required this.id,
    required this.name,
    required this.status,
    required this.species,
    required this.type,
    required this.gender,
    required this.origin,
    required this.location,
    required this.image,
    required this.episodes,
    required this.url,
    required this.created
  });

  factory CharacterInfo.fromJson(Map<String, dynamic> json) {
    return CharacterInfo(
      id: json['id'] as int? ?? 0, 
      name: json['name'] as String? ?? '', 
      status: json['status'] as String? ?? '', 
      species: json['species'] as String? ?? '',
      type: json['type'] as String? ?? '',
      gender: json['gender'] as String? ?? '',
      origin: Location.fromJson(json["origin"]),
      location: Location.fromJson(json["location"]),
      image: json['image'] as String? ?? '',
      episodes: List<String>.from(json["episode"].map((x) => x)),
      url: json['url'] as String? ?? '',
      created: DateTime.parse(json["created"])
    );
  }

  @override
  List<Object?> get props => [id, name, status, species, type, gender, origin, location, image, episodes, url, created];
}

class Location extends Equatable {
  final String name;
  final String url;

  const Location({
    required this.name,
    required this.url
  });

  factory Location.fromJson(Map<String, dynamic> json) {
    return Location(
      name: json['name'] as String? ?? '', 
      url: json['url'] as String? ?? ''
    );
  }

  Map<String, dynamic> toJson() => {
    "name": name,
    "url": url,
  };

  @override
  List<Object?> get props => [name, url];
}
