import 'package:dio/dio.dart';
import 'package:first_app/source/features/rick_and_morty/data/models/characters.dart';

class APIRemoteSource {
  APIRemoteSource(this._httpClient);

  final Dio _httpClient;
  final String _baseURL = 'https://rickandmortyapi.com/api';

  Future<CharactersList> getCharacters() async {
    try {
      final response = await _httpClient.get<Map<String, dynamic>>(
        '$_baseURL/character',
        options: Options(
          responseType: ResponseType.plain,
          headers: {
            'Content-type': 'application/json'
          }
        )
      );

      if (response.statusCode == 200) {
        final body = response.data ?? {};

        final result = CharactersList.fromJson(body);
        return await Future.delayed(
          const Duration(seconds: 2), () {
            return result;
          }
        );
      } else {
        throw Exception(response.statusMessage);
      }
    } on Exception catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<CharactersList> getMoreCharacters(String url) async {
    try {
      final response = await _httpClient.get<Map<String, dynamic>>(
        url,
        options: Options(
          responseType: ResponseType.plain,
          headers: {
            'Content-type': 'application/json'
          }
        )
      );

      if (response.statusCode == 200) {
        final body = response.data ?? {};

        final result = CharactersList.fromJson(body);
        return await Future.delayed(
          const Duration(seconds: 2), () {
            return result;
          }
        );
      } else {
        throw Exception(response.statusMessage);
      }
    } on Exception catch (e) {
      throw Exception(e.toString());
    }
  }
}