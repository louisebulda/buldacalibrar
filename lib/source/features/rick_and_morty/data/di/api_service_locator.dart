import 'package:dio/dio.dart';
import 'package:first_app/source/features/rick_and_morty/data/datasource/remote/api_remote_source.dart';
import 'package:first_app/source/features/rick_and_morty/data/repository/api_repository_implem.dart';

final httpClient = Dio();
final apiRemoteSource = APIRemoteSource(httpClient);
final apiRepository = APIRepositoryImplem(apiRemoteSource);