import 'dart:async';

import 'package:first_app/source/features/rick_and_morty/data/datasource/remote/api_remote_source.dart';
import 'package:first_app/source/features/rick_and_morty/data/models/characters.dart';
import 'package:first_app/source/features/rick_and_morty/data/models/page_info.dart';
import 'package:first_app/source/features/rick_and_morty/data/repository/api_repository.dart';

class APIRepositoryImplem extends APIRepository {
  APIRepositoryImplem(this._remoteSource);
  final APIRemoteSource _remoteSource;

  final StreamController<CharactersList> _streamController = 
  StreamController.broadcast();

  CharactersList _characters = CharactersList(
    pageInfo: const PageInfo(count: 0, pages: 0, next: '', prev: ''), 
    results: const []
  );

  @override
  Stream<CharactersList> getCharacters() => _streamController.stream;

  @override
  Future<CharactersList> load() async {
    final charactersList = await _remoteSource.getCharacters();
    _characters = charactersList;
    _streamController.add(_characters);

    return charactersList;
  }

  @override
  Future<CharactersList> fetchCharacters(String url) async {
    if (url.isNotEmpty) {
      final charactersList = await _remoteSource.getMoreCharacters(url);

      _characters = charactersList;
      _streamController.add(_characters);

      return charactersList;
    } else {
      final charactersList = await _remoteSource.getCharacters();
      _characters = charactersList;
      _streamController.add(_characters);

      return charactersList;
    }
  }
}