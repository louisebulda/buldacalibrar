import 'package:first_app/source/features/rick_and_morty/data/models/characters.dart';

abstract class APIRepository {
  Stream<CharactersList> getCharacters();
  Future<CharactersList> load();
  Future<CharactersList> fetchCharacters(String url);
}