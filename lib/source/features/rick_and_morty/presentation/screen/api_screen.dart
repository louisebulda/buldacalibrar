import 'package:first_app/source/features/rick_and_morty/data/models/character_info.dart';
import 'package:first_app/source/features/rick_and_morty/presentation/view_model/api_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:google_fonts/google_fonts.dart';

class APIScreen extends StatefulWidget {
  APIScreen({super.key});

  @override
  _APIScreenState createState() => _APIScreenState();
}

class _APIScreenState extends State<APIScreen> {
  final ScrollController _scrollController = ScrollController();
  late APIViewModel _apiViewModel;
  String nextPage = '';
  String prevPage = '';

  @override
  void initState() {
    super.initState();

    _apiViewModel = BlocProvider.of<APIViewModel>(context);
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _loadMoreItems();
      }

      if (_scrollController.position.pixels ==
          _scrollController.position.minScrollExtent) {
        _loadPrevItems();
      }
    });
  }

  void _loadMoreItems() {
    _apiViewModel.add(FetchCharacters(nextPage));
  }

  void _loadPrevItems() {
    _apiViewModel.add(FetchCharacters(prevPage));
  }

  @override
  Widget build(BuildContext context) {
    if (kIsWeb) {
      return Center(
        child: Padding(
          padding: EdgeInsets.zero,
          child: BlocBuilder<APIViewModel, APIState>(
                buildWhen: (previous, current) => current.status != previous.status,
                builder: (context, state) {
                  if (state.status == CharacterStatus.loading) {
                    return const Center(
                      child: Padding(
                        padding: EdgeInsets.all(16.0),
                        child: CircularProgressIndicator(),
                      ),
                    );
                  }
                    
                  return GridView.builder(
                    controller: _scrollController,
                    scrollDirection: Axis.vertical,
                    itemCount: state.characters.length,
                    padding: const EdgeInsets.all(16.0),
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      final character = state.characters[index];
                      nextPage = state.pageInfo.next;
                      prevPage = state.pageInfo.prev;
                      
                      return ListTile(
                        title: Column(
                          children: [
                            Image.network(character.image ?? ''),
                            Text(character.name ?? '', style: TextStyle(
                              fontFamily: GoogleFonts.josefinSans().fontFamily,
                              fontWeight: FontWeight.normal,
                              fontSize: 16,
                              color: Colors.black,
                              wordSpacing: 1.3)
                            ),
                          ],
                        ),
                      );
                    },
                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      childAspectRatio: 0.75,
                    ),
                  );
                })
        ),
      );
    } else {
      return Padding(
        padding: EdgeInsets.zero,
        child: BlocBuilder<APIViewModel, APIState>(
          buildWhen: (previous, current) => current.status != previous.status,
          builder: (context, state) {
            if (state.status == CharacterStatus.loading) {
              return const Center(
                child: Padding(
                  padding: EdgeInsets.all(16.0),
                  child: CircularProgressIndicator(),
                ),
              );
            }
                  
            return ListView.builder(
              controller: _scrollController,
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              padding: const EdgeInsets.all(16.0),
              itemBuilder: (context, index) {
                final character = state.characters[index];
                nextPage = state.pageInfo.next;
                prevPage = state.pageInfo.prev;
                
                return ListTile(
                  title: Column(
                    children: [
                      Image.network(character.image ?? ''),
                      Text(character.name ?? '', style: TextStyle(
                        fontFamily: GoogleFonts.josefinSans().fontFamily,
                        fontWeight: FontWeight.normal,
                        fontSize: 16,
                        color: Colors.black,
                        wordSpacing: 1.3)
                      ),
                    ],
                  )
                );
              },
              itemCount: state.characters.length,
            );
          }),
        );
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}