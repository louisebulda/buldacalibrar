import 'package:equatable/equatable.dart';
import 'package:first_app/source/features/rick_and_morty/data/models/character_info.dart';
import 'package:first_app/source/features/rick_and_morty/data/models/characters.dart';
import 'package:first_app/source/features/rick_and_morty/data/models/page_info.dart';
import 'package:first_app/source/features/rick_and_morty/data/repository/api_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part '../event/api_event.dart';
part '../state/api_state.dart';

class APIViewModel extends Bloc<APIEvent, APIState> {
  APIViewModel({
    required APIRepository repository,
  }) : _repository = repository,
      super(const APIState()) {
        on<APISubscriptionRequested>(_onSubscriptionRequested);
        on<CharactersRequested>(_onCharactersRequested);
        on<FetchCharacters>(_onFetchCharacters);
        on<FetchPrevCharacters>(_onFetchPrevCharacters);
      }

  final APIRepository _repository;

  Future<void> _onFetchCharacters(
    FetchCharacters event,
    Emitter<APIState> emit
  ) async {
    try {
      emit(state.copyWith(status: CharacterStatus.loading));

      final response = await _repository.fetchCharacters(event.next);

      emit(state.copyWith(
        status: CharacterStatus.success,
        characters: response.results?.toList() ?? [],
        pageInfo: response.pageInfo,
      ));
    } on Exception catch (_) {
      state.copyWith(status: CharacterStatus.error);
    }
  }

  Future<void> _onFetchPrevCharacters(
    FetchPrevCharacters event,
    Emitter<APIState> emit
  ) async {
    try {
      emit(state.copyWith(status: CharacterStatus.loading));

      final response = await _repository.fetchCharacters(event.prev);

      emit(state.copyWith(
        status: CharacterStatus.success,
        characters: response.results?.toList() ?? [],
        pageInfo: response.pageInfo,
      ));
    } on Exception catch (_) {
      state.copyWith(status: CharacterStatus.error);
    }
  }
  
  Future<void> _onSubscriptionRequested(
    APISubscriptionRequested event,
    Emitter<APIState> emit,
  ) async {
    emit(state.copyWith(status: CharacterStatus.loading));

    await emit.forEach<CharactersList> (
      _repository.getCharacters(),
      onData: (response) {
        return state.copyWith(
          status: CharacterStatus.success,
          characters: [
            ...state.characters,
            ...response.results?.toList() ?? []
          ],
          pageInfo: response.pageInfo
        );
      }, 
      onError: (_, __) => state.copyWith(
        status: CharacterStatus.error
      )
    );
  }

  Future<void> _onCharactersRequested(
    CharactersRequested event,
    Emitter<APIState> emit,
  ) async {
    try {
      final response = await _repository.load();

      emit(state.copyWith(
        status: CharacterStatus.success,
        characters: response.results?.toList() ?? [],
        pageInfo: response.pageInfo
      ));
    } on Exception catch (_) {
      state.copyWith(status: CharacterStatus.error);
    }
  }
}