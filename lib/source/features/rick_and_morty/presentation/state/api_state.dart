part of '../view_model/api_view_model.dart';

class APIState extends Equatable {
  const APIState({
    this.pageInfo = const PageInfo(count: 0, pages: 0, next: '', prev: ''),
    this.characters = const [],
    this.status = CharacterStatus.initial,
  });

  final PageInfo pageInfo;
  final CharacterStatus status;
  final List<CharacterInfo> characters;

  @override
  List<Object?> get props => [pageInfo, characters, status];

  APIState copyWith({
    CharacterStatus? status,
    PageInfo? pageInfo,
    List<CharacterInfo>? characters,
  }) {
    return APIState(
      status: status ?? this.status,
      pageInfo: pageInfo ?? this.pageInfo,
      characters: characters ?? this.characters,
    );
  }
}

enum CharacterStatus { initial, loading, success, error }