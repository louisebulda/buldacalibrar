part of '../view_model/api_view_model.dart';

abstract class APIEvent {}

class APISubscriptionRequested extends APIEvent {}

class CharactersRequested extends APIEvent {}

class FetchCharacters extends APIEvent {
  final String next;

  FetchCharacters(this.next);
}

class FetchPrevCharacters extends APIEvent {
  final String prev;

  FetchPrevCharacters(this.prev);
}

