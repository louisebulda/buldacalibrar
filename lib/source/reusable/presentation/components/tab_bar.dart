import 'package:first_app/source/features/blog/presentation/screen/blog_post_screen.dart';
import 'package:first_app/source/features/rick_and_morty/data/di/api_service_locator.dart';
import 'package:first_app/source/features/rick_and_morty/presentation/screen/api_screen.dart';
import 'package:first_app/source/features/rick_and_morty/presentation/view_model/api_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

class CalibrarTabBar extends StatelessWidget {
  const CalibrarTabBar({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
		  length: 2,
		  child: Padding(
              padding: EdgeInsets.zero,
              child: Scaffold(
                body: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: NestedScrollView(
                    headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
                      return <Widget> [
                        SliverAppBar(
                          pinned: true,
                          floating: true,
                          surfaceTintColor: Colors.transparent,
                          centerTitle: false,
                          title: Row (
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              const Text("Calibrar"),
                              TabBar(
                                isScrollable: true,
                                labelStyle: TextStyle(
                                  fontFamily: GoogleFonts.josefinSans().fontFamily
                                ),
                                dividerColor: Colors.transparent,
                                overlayColor: MaterialStateProperty.all(Colors.transparent),
                                labelColor: Colors.white,
                                unselectedLabelColor: Colors.black,
                                labelPadding: const EdgeInsets.only(right: 16, left: 16),
                                indicatorSize: TabBarIndicatorSize.tab,
                                indicatorPadding: EdgeInsets.zero,
                                indicator: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  color: Colors.deepPurple
                                ),
                                tabs: const [
                                  Tab(child: Text("Blog")),
                                  Tab(child: Text("Rick & Morty API"))
                                ]
                              )
                            ],
                          ),
                          
                          titleTextStyle: TextStyle(
                            fontFamily: GoogleFonts.lobster().fontFamily,
                            fontSize: 24,
                            color: Colors.black
                          ),
                        )
                      ];
                    }, 
                  
                    body: TabBarView(
                            // Change to respective screens
                            children: <Widget>[
                              const BlogPostScreen(),
                              BlocProvider(
                                create: (context) => APIViewModel(repository: apiRepository)
                                ..add(APISubscriptionRequested())
                                ..add(CharactersRequested()),
                                child: APIScreen(),
                              )
                            ],
                      ),
                  ),
                )
              ),
        ), // Scaffold
      ), // DefaultTabController
    ); // MaterialApp
}
}
