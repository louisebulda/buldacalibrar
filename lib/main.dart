// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables
import 'package:first_app/source/reusable/presentation/components/tab_bar.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const CalibrarProject());
}

class CalibrarProject extends StatelessWidget {
  const CalibrarProject({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Bulda Calibrar Project",
      theme: ThemeData(
        primarySwatch: Colors.blue,
        fontFamily: GoogleFonts.josefinSans().fontFamily
      ),
      home: const CalibrarTabBar(),
    );
  }
}
